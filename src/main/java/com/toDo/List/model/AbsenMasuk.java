package com.toDo.List.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.toDo.List.enumated.AbsenEnum;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;

@Entity
@Table(name = "absen_masuk")
public class AbsenMasuk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonFormat(pattern = " HH:mm:ss")
    @Column(name = "jam_masuk")
    private Date jam;

    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MMM-dd")
    @Column(name = "tanggal_masuk")
    private Date tanggal;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserModel userId;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "keterangan")
    private AbsenEnum keterangan;

    public AbsenMasuk() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getJam() {
        return jam;
    }

    public void setJam(Date jam) {
        this.jam = jam;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public UserModel getUserId() {
        return userId;
    }

    public void setUserId(UserModel userId) {
        this.userId = userId;
    }

    public AbsenEnum getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(AbsenEnum keterangan) {
        this.keterangan = keterangan;
    }
}

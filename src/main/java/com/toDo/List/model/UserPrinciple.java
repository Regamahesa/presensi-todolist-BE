package com.toDo.List.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserPrinciple implements UserDetails {

    private String email;

    private String password;


    public static UserPrinciple build(UserModel userModel) {
    var role =Collections.singletonList(new SimpleGrantedAuthority(userModel.getRole().name()));
    return new UserPrinciple(
            userModel.getEmail(),
            userModel.getPassword(),
            role
    );
    }

    private Collection<? extends GrantedAuthority> authority;
    public UserPrinciple(String email, String password, Collection<? extends GrantedAuthority> authority) {
        this.email = email;
        this.password = password;
        this.authority = authority;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authority;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}

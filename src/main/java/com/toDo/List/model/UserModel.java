package com.toDo.List.model;

import com.toDo.List.enumated.UserEnum;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "alamat")
    private String alamat;

    @Lob
    @Column(name = "foto")
    private String foto;

    @Column(name = "no_telepon")
    private String noTelepon;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserEnum role;

    public UserModel() {
    }

    public UserModel( String username, String email, String password, String alamat, String fotoProfile, String noTelepon, UserEnum role) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.alamat = alamat;
        this.foto = fotoProfile;
        this.noTelepon = noTelepon;
        this.role = role;
    }

    public UserEnum getRole() {
        return role;
    }

    public void setRole(UserEnum role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNoTelepon() {
        return noTelepon;
    }

    public void setNoTelepon(String noTelepon) {
        this.noTelepon = noTelepon;
    }
}

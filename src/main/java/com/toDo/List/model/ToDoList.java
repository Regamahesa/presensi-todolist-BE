package com.toDo.List.model;

import javax.persistence.*;

@Entity
@Table(name = "list")
public class ToDoList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "tugas")
    private String tugas;

    @Column(name = "deskripsi")
    private String deskripsi;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserModel userId;

    public ToDoList() {
    }

    public ToDoList(int id, String tugas, String deskripsi, UserModel userId) {
        this.id = id;
        this.tugas = tugas;
        this.deskripsi = deskripsi;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTugas() {
        return tugas;
    }

    public void setTugas(String tugas) {
        this.tugas = tugas;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public UserModel getUserId() {
        return userId;
    }

    public void setUserId(UserModel userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ToDoList{" +
                "id=" + id +
                ", tugas='" + tugas + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", userId=" + userId +
                '}';
    }
}

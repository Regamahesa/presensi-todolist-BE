package com.toDo.List.controller;

import com.toDo.List.dto.LoginDto;
import com.toDo.List.dto.UserDto;
import com.toDo.List.model.UserModel;
import com.toDo.List.response.CommonResponse;
import com.toDo.List.response.ResponHelper;
import com.toDo.List.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/acount")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String ,Object>> login(@RequestBody LoginDto loginDto) {
    return ResponHelper.ok(userService.Login(loginDto));
    }

    @PostMapping(path = "/sign-up", consumes = "multipart/form-data")
    public CommonResponse<UserModel> Registrasi(UserDto userDto, @RequestPart("file")MultipartFile multipartFile) {
        return ResponHelper.ok(userService.addUsers(modelMapper.map(userDto, UserModel.class), multipartFile));
    }

    @GetMapping("/all")
    public CommonResponse<List<UserModel>> getAllUser() {
      return ResponHelper.ok(userService.getAllUser());
    }
    @PutMapping( path = "/{id}",consumes = "multipart/form-data")
    public CommonResponse<UserModel> editUsers(@PathVariable("id")Integer id, UserDto userDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponHelper.ok(userService.editUser(id, modelMapper.map(userDto, UserModel.class), multipartFile));
    }
    @GetMapping("/{id}")
    public CommonResponse<UserModel> getUserById(@PathVariable("id") Integer id) {
        return ResponHelper.ok(userService.getUserById(id));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<?> deleteUsers(@PathVariable("id")Integer id) {
        return ResponHelper.ok(userService.deleteUsers(id));
    }

}

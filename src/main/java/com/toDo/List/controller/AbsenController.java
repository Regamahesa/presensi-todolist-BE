package com.toDo.List.controller;

import com.toDo.List.dto.AbsenMasukDto;
import com.toDo.List.model.AbsenMasuk;
import com.toDo.List.repository.MasukRepository;
import com.toDo.List.response.CommonResponse;
import com.toDo.List.response.ResponHelper;
import com.toDo.List.service.AbsenMasukService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/absen")
public class AbsenController {

    @Autowired
    private AbsenMasukService absenMasukService;

    @PostMapping("/absen-masuk")
    public CommonResponse<AbsenMasuk> absenMasuk(@RequestBody AbsenMasukDto absenMasukDto) {
    return ResponHelper.ok(absenMasukService.absenMasuk(absenMasukDto));
    }

    @PostMapping("/absen-pulang")
    public CommonResponse<AbsenMasuk> absenPulang(@RequestBody AbsenMasukDto absenMasukDTO) {
        return ResponHelper.ok(absenMasukService.absenPulang(absenMasukDTO));
    }

    @GetMapping
    public CommonResponse<List<AbsenMasuk>> findAll(@RequestParam int page, Integer userId){
        return ResponHelper.ok(absenMasukService.findAll(userId, page));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<?>  delete(@PathVariable("id") int id) {
        return ResponHelper.ok(absenMasukService.deleteAbsen(id));
    }

}

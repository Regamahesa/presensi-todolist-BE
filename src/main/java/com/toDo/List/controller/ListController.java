package com.toDo.List.controller;

import com.toDo.List.dto.ListDto;
import com.toDo.List.model.ToDoList;
import com.toDo.List.response.CommonResponse;
import com.toDo.List.response.ResponHelper;
import com.toDo.List.service.ListService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/list")
public class ListController {

    @Autowired
    private ListService listService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public CommonResponse<ToDoList> addList( ListDto listDto) {
    return ResponHelper.ok(listService.addlist(listDto));
    }

    @GetMapping
    public CommonResponse<Page<ToDoList>> getAllList(@RequestParam int page, Integer userId) {
    return ResponHelper.ok(listService.getAllList(page, userId));
    }

    @GetMapping("/{id}")
    public CommonResponse<ToDoList> getById(@PathVariable("id") Integer id){
        return ResponHelper.ok(listService.getListById(id));
    }

    @PutMapping("/{id}")
    public CommonResponse<ToDoList> editList(@PathVariable("id") int id,@RequestBody ListDto list) {
    return ResponHelper.ok(listService.editList(id, list));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String , Object>> deleteList(@PathVariable("id") int id) {
        return ResponHelper.ok(listService.deleteList(id)); 
    }
}

package com.toDo.List.service_impl;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.toDo.List.dto.LoginDto;
import com.toDo.List.enumated.UserEnum;
import com.toDo.List.exception.InternalErrorException;
import com.toDo.List.exception.NotFoundException;
import com.toDo.List.jwt.JwtProvider;
import com.toDo.List.model.UserModel;
import com.toDo.List.repository.UserRepository;
import com.toDo.List.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/upload-image-examle.appspot.com/o/%s?alt=media";

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtention(multipartFile.getOriginalFilename());
            File file = convertToFile (multipartFile, fileName);
            var RESPONSE_URL =uploadFile( file, fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error Upload File");
        }
    }
    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }
    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("upload-image-examle.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtention(String filename) {
        return filename.split("\\.")[0];
    }

    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Password Not Found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> Login(LoginDto loginDto) {
    String token = authories(loginDto.getEmail(), loginDto.getPassword());
    UserModel user = userRepository.findByEmail(loginDto.getEmail()).get();
    Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("user", user);
        return response;
    }

    @Override
    public UserModel addUsers(UserModel userModel, MultipartFile multipartFile) {
        String foto = imageConverter(multipartFile);
    UserModel addUser = new UserModel(userModel.getUsername(), userModel.getEmail(), passwordEncoder.encode(userModel.getPassword()), userModel.getAlamat(), foto ,userModel.getNoTelepon(), userModel.getRole());
    String email = userModel.getEmail();
    userModel.setPassword(passwordEncoder.encode(userModel.getPassword()));
    if (userModel.getRole().name().equals("USER"))
        userModel.setRole(UserEnum.USER);
        var validasi = userRepository.findByEmail(email);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf Email sudah digunakan");
        }
        return userRepository.save(addUser);
    }

    @Override
    public List<UserModel> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public UserModel getUserById(Integer id) {
        var respon = userRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Not Found"));
        try {
            return userRepository.save(respon);
        }catch (Exception e) {
            throw new InternalErrorException("Kesalahan Memunculkan Data");
        }

    }

    @Override
    public UserModel editUser(Integer id, UserModel userModel, MultipartFile multipartFile){
    UserModel update = userRepository.findById(id).orElseThrow(() -> new NotFoundException("id tidak ada"));
    var validasi = userRepository.findByEmail(userModel.getEmail());
    if (validasi.isPresent()){
        throw new InternalErrorException("Maaf email sudah ada");
    }
    String foto = imageConverter(multipartFile);
    update.setUsername(userModel.getUsername());
    update.setEmail(userModel.getEmail());
    update.setPassword(passwordEncoder.encode(userModel.getPassword()));
    update.setAlamat(userModel.getAlamat());
    update.setNoTelepon(userModel.getNoTelepon());
    update.setFoto(foto);
    update.setRole(userModel.getRole());
        return userRepository.save(update);
    }

    @Override
    public Map<String, Boolean> deleteUsers(Integer id) {
        try {
            userRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}

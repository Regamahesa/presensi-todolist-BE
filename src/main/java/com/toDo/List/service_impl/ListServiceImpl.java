package com.toDo.List.service_impl;

import com.toDo.List.dto.ListDto;
import com.toDo.List.exception.NotFoundException;
import com.toDo.List.model.ToDoList;
import com.toDo.List.repository.ToDoListRepository;
import com.toDo.List.repository.UserRepository;
import com.toDo.List.service.ListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ListServiceImpl implements ListService {

    @Autowired
    private ToDoListRepository listRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ToDoList addlist(ListDto listDto) {
        ToDoList create = new ToDoList();
        create.setTugas(listDto.getTugas());
        create.setDeskripsi(listDto.getDeskripsi());
        create.setUserId(userRepository.findById(listDto.getUserId()).orElseThrow(() -> new NotFoundException("user id tidak ditemukan")));
        return listRepository.save(create);
    }

    @Override
    public Page<ToDoList> getAllList(int page, Integer userId) {
        Pageable pageable = PageRequest.of(page, 5);
        return listRepository.findAllByUserId(userId, pageable);
    }

    @Override
    public ToDoList getListById(Integer id) {
        return listRepository.findById(id).orElseThrow(() -> new NotFoundException("Id tidak ada"));
    }

    @Override
    public ToDoList editList(Integer id, ListDto list) {
           ToDoList update = listRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Tidak ada"));
            update.setTugas(list.getTugas());
            update.setDeskripsi(list.getDeskripsi());
        return listRepository.save(update);
    }

    @Override
    public Map<String, Object> deleteList(Integer id) {
        listRepository.deleteById(id);
        Map<String, Object> obj = new HashMap<>();
        obj.put("DELETED", true);
        return obj;
    }
}

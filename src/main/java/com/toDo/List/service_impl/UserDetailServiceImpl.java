package com.toDo.List.service_impl;

import com.toDo.List.model.UserModel;
import com.toDo.List.model.UserPrinciple;
import com.toDo.List.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel users = userRepository.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("Username tidak ada"));
        return UserPrinciple.build(users);
    }
}

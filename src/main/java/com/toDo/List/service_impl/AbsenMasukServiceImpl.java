package com.toDo.List.service_impl;

import com.toDo.List.dto.AbsenMasukDto;
import com.toDo.List.enumated.AbsenEnum;
import com.toDo.List.exception.NotFoundException;
import com.toDo.List.model.AbsenMasuk;
import com.toDo.List.repository.MasukRepository;
import com.toDo.List.repository.UserRepository;
import com.toDo.List.service.AbsenMasukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AbsenMasukServiceImpl implements AbsenMasukService {
    private static final Integer hour = 3600 * 1000;

    @Autowired
    private MasukRepository masukRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public AbsenMasuk absenMasuk(AbsenMasukDto absenMasukDto) {
        AbsenMasuk create = new AbsenMasuk();
        create.setJam(new Date(new Date().getTime() + 7 * hour));
        create.setUserId(userRepository.findById(absenMasukDto.getUserId()).orElseThrow(() -> new NotFoundException("Id not found")));
        create.setKeterangan(AbsenEnum.MASUK);
        return masukRepository.save(create);
    }

    @Override
    public AbsenMasuk absenPulang(AbsenMasukDto absenMasukDto) {
        AbsenMasuk create = new AbsenMasuk();
        create.setJam(new Date(new Date().getTime() + 7 * hour));
        create.setUserId(userRepository.findById(absenMasukDto.getUserId()).orElseThrow(() -> new NotFoundException("Id not found")));
        create.setKeterangan(AbsenEnum.PULANG);
        return masukRepository.save(create);
    }

    @Override
    public List<AbsenMasuk> findAll(Integer userId, int page) {
        Pageable pageable = PageRequest.of(page, 5);
        return masukRepository.findUser(userId, pageable);
    }



    @Override
    public Map<String, Boolean> deleteAbsen(int id) {
        try {
            masukRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }

    }
}

package com.toDo.List.dto;

import com.toDo.List.enumated.UserEnum;

public class LoginDto {

    private String email;
    private String password;
    private UserEnum role;

    public UserEnum getRole() {
        return role;
    }

    public void setRole(UserEnum role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package com.toDo.List.dto;

import com.toDo.List.enumated.AbsenEnum;

public class AbsenMasukDto {

    private int userId;
    private AbsenEnum keterangan;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public AbsenEnum getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(AbsenEnum keterangan) {
        this.keterangan = keterangan;
    }
}

package com.toDo.List.repository;

import com.toDo.List.model.ToDoList;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoListRepository extends JpaRepository<ToDoList, Integer> {

    @Query(value = "SELECT * FROM list WHERE user_id = ?1", nativeQuery = true)
    Page<ToDoList> findAllByUserId(Integer userId, Pageable pageable);
}

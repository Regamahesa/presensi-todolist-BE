package com.toDo.List.repository;

import com.toDo.List.model.TemporaryToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TemporaryRepository extends JpaRepository<TemporaryToken, Integer> {
    Optional<TemporaryToken> findByToken(String token);
}

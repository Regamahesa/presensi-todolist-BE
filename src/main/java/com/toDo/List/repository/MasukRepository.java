package com.toDo.List.repository;

import com.toDo.List.model.AbsenMasuk;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MasukRepository extends JpaRepository<AbsenMasuk, Integer> {

    @Query(value = "SELECT * FROM absen_masuk  WHERE user_id = ?1", nativeQuery = true)
    List<AbsenMasuk> findUser(Integer userId, Pageable pageable);
}

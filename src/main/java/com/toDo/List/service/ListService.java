package com.toDo.List.service;

import com.toDo.List.dto.ListDto;
import com.toDo.List.model.ToDoList;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface ListService {

    ToDoList addlist(ListDto listDto);

    Page<ToDoList> getAllList(int page, Integer userId);

    ToDoList getListById(Integer id);

    ToDoList editList(Integer id, ListDto list);

    Map<String, Object> deleteList(Integer id);

}

package com.toDo.List.service;

import com.toDo.List.dto.LoginDto;
import com.toDo.List.model.UserModel;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
public interface UserService {

    Map<String, Object> Login(LoginDto loginDto);

    UserModel addUsers(UserModel userModel, MultipartFile multipartFile);

    List<UserModel> getAllUser();

    UserModel getUserById(Integer id);

    UserModel editUser(Integer id, UserModel userModel, MultipartFile multipartFile);

    Map<String, Boolean> deleteUsers(Integer id);
}

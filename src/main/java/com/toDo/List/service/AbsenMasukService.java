package com.toDo.List.service;

import com.toDo.List.dto.AbsenMasukDto;
import com.toDo.List.model.AbsenMasuk;

import java.util.List;
import java.util.Map;

public interface AbsenMasukService {

    AbsenMasuk absenMasuk(AbsenMasukDto absenMasukDto);
    AbsenMasuk absenPulang(AbsenMasukDto absenMasukDto);

    List<AbsenMasuk> findAll(Integer userId, int page);

    Map<String, Boolean> deleteAbsen(int id);

}
